//cse02 Helen Borchart Hw 07. This code uses different methods to calculate the number of white spaces, how many instances
//a word is found (findText), the number of words, repkalcing "!" with "." and shortening spaces (if 2+ spaces are found,
//making it only 1 space) and then an option to quit.

import java.util.Scanner;

public class WordTools{
  public static void main(String[]args){
    Scanner myScanner = new Scanner(System.in);
    String paragraph = sampleText(myScanner);
    String choice;   
  do{ //do while loop
    choice = printMenu(myScanner); //gets choice input from the user 
    if(choice.equals("q")) //if the user enters q then they want to quit but you continue to go through the loop
      //bc the while statement makes sure that choice does not equal q
         continue;
    else if(choice.equals("c")) //checks to see what the choice is 
        System.out.println("The number of non-white space characters is " + getNumOfNonWSCharacters(paragraph));
    else if(choice.equals("w")) //checks to see what the choice is 
         System.out.println("The number of words is " + getNumWords(paragraph));
    else if(choice.equals("f")){ //checks to see what the choice is 
      myScanner.nextLine(); //clears the buffer from the choice 
      System.out.println("Enter the word you want to find");
      String find = myScanner.next(); //gets input from the user as to what word they want to find 
       System.out.println("The word appears " + findText(paragraph, find));  
    }
    else if(choice.equals("r")) //checks users choice 
      System.out.println("Edited text: " + replaceExclamation(paragraph));
      
    else if(choice.equals("s")) //checks users choice 
         System.out.println("Edited text: " + shortenSpace(paragraph));
    else
      System.out.println("Invalid input"); //if the user does not enter any of the letters, it returns invalid input
  
  } while(!choice.equals("q"));//quits  

}
  
  public static String sampleText(Scanner scanner){ //gets a paragraph from the user
    System.out.println("Enter a sample text: ");
    String paragraph = scanner.nextLine(); //assigns the input to the variable paragraph
    System.out.println("You entered " + paragraph);
    return paragraph; //returns paragraph 
  }
  
  public static String printMenu(Scanner scanner){ //prints the menu 
    System.out.println("Menu"); //print statement 
    System.out.println("c - Number of non-whitespace characters"); //print statement 
    System.out.println("w - Number of words"); //print statement 
    System.out.println("f - Find text"); //print statement 
    System.out.println("r - Replace all !'s"); //print statement 
    System.out.println("s - Shorten spaces"); //print statement 
    System.out.println("q - quit"); //print statement 
    System.out.println("Choose an option"); //print statement 
    String choice = scanner.next(); //assigns the users choice to the variable choice 
    return choice;
    }
  
  
  public static int getNumWords(String paragraph){ //finds the number of words in the paragraph
    int count = 0; //variable count 
    for(int i = 0; i <= paragraph.length()-1; i++){ //goes through the whole paragraph
      if(paragraph.charAt(i) == ' ' && paragraph.charAt(i + 1) != ' ') //if the loop is at a space or the loop is at a 
        //position where the next character is not a space it is considered a word
        count++; //counter gets added to 
    }
    return count +1; //returns the counter 
  }
  public static int findText(String toBeFound, String userProvided){ //finds how many times a word appears in the text 
    int count = 0; //variable counter 
    while(toBeFound.contains(userProvided)){ //while loop that goes through as long as the word is found in the text
        toBeFound = toBeFound.substring(toBeFound.indexOf(userProvided) + userProvided.length()); 
        count++;//if they are the same then the counter increases
      }
return count;//return statement 
}
    
  
  public static String replaceExclamation(String replacedString){ //replaces all !'s with .'s
    replacedString = replacedString.replace("!", "."); //replace statememt 
      return replacedString; //return statememt 
  
  } 
  
  public static String shortenSpace(String shortenedString){ //shortens multiple spaces to only one space 
    for(int i = 0; i < shortenedString.length(); i++){ //for loop that goes through 
      if(shortenedString.charAt(i) == ' ' && shortenedString.charAt(i + 1) == ' ') //if statement to check to see if 
        //there are multiple spaces
        shortenedString = shortenedString.substring(0, i) + shortenedString.substring(i + 1); //new string    
    }  
    
   return shortenedString; //return statement  
  }
  
  public static int getNumOfNonWSCharacters(String characters ){ //finds the number of non white space characters
    int count = 0; //counter variable 
    for(int i = 0; i < characters.length(); i++){ //for loop to go through the paragraph
      if(characters.charAt(i) != ' '){ //checks to see that it is not a white space
        count++; //counter increases
      }
    
    }
    return count;
  }
  
}
 