//cse2 section 210. Helen Borchart. This uses nested loops to print out variations of different patterns. This is pattern
//b and prints out 1 2 3 4 5 6 , then 1 2 3 4 5  on the next line and so on until it reaches the lowest value.


import java.util.Scanner;
public class PatternB{
  public static void main (String[]args){
    Scanner myScanner = new Scanner(System.in); //creates scanner
    int userInput = 0; //creates variable to hold user input
    String junk = ""; //creates a string to hold junk if the user does not enter valid input
    System.out.println("Enter an int 1-10");
    while(!myScanner.hasNextInt()){ //loop that executes if the scanner receives bad input
      junk = myScanner.nextLine(); //clears buffer
      System.out.println("Error, enter an integer"); 
    }
    while(myScanner.hasNextInt()){ //loop that executes if there is valid input
      userInput = myScanner.nextInt(); //assigns the user input to the variable
      if(userInput< 1 || userInput > 10) //checks to see that the user input is between 1 and 10
      System.out.println("Error, enter an int between 1-10"); //error message if the input is not between 1- 10
      if(userInput >= 1  && userInput <= 10){ //if statement that executes if the input is valid 
        break;  //breaks out of the loop
      }
        
    }
for(int i = userInput; i >= 1; i--){ //outer for loop that controls the number of rows 
  for(int j = 1; j <= i; j++){ //inner for loop that controls the print statements 
    System.out.print(j + " "); //print statement 
    }  
System.out.println(); //new line
      
    }   
  }
}