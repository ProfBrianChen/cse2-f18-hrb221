//cse 02. September 5, 2018. lab02. Section 210. Cyclometer class. Helen Borchart. This program calculates different types of data from a bicyle meter and prints different information from the meter.
public class Cyclometer{
  public static void main(String[]args){
    int secsTrip1 = 480; //variable for the number of seconds for the first trip
    int secsTrip2 = 3220; //variable for the number of seconds from the second trip
    int countsTrip1 = 1561; //variable for the number of wheel rotations from the first trip
    int countsTrip2 = 9037; //variable for the number of wheel rotations from the second trip
    double wheelDiameter = 27.0; //the variable for the Wheel Diameter 
    double PI = 3.14159;
    double feetPerMile = 5280; //variable declaring that there are 5280 feet in a mile
    double inchesPerFoot = 12; //varibale declaring that there are 12 inches in a foot 
    double secondsPerMinute = 60; //variable declaring that there are 60 seconds in a minute 
    double distanceTrip1; //a variable for the distance of the first trip
    double distanceTrip2; //a variable for the distance of the second trip
    double totalDistance; //a variable for the distance of both trips total 
    
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute)+ " minutes and had " + countsTrip1 + " counts.");
    System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute)+ " minutes and had " + countsTrip2 + " counts.");
    distanceTrip1 = countsTrip1 * wheelDiameter * PI;
    distanceTrip1/=inchesPerFoot * feetPerMile;
    distanceTrip2 = countsTrip2 * wheelDiameter * PI/inchesPerFoot/feetPerMile;
    totalDistance = distanceTrip1 + distanceTrip2;
    System.out.println("Trip 1 was " + distanceTrip1 + " miles");
    System.out.println("Trip 2 was " + distanceTrip2 + " miles");
    System.out.println("The total distance was " + totalDistance + " miles");
  }
}
