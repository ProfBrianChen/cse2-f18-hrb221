//cse02 lab07 Helen Borchart. This uses methods to hold different types of words like adjectives, non primary nouns, past
//tense verbs and non primary nouns appropriate for the object of a sentence.

import java.util.Random;
import java.util.Scanner;

public class lab07{
  public static void main(String[]args){
  Random randomGen = new Random(); //creates a new random object
  int randomInt = randomGen.nextInt(10); //generates a random int between 0-10
  Scanner myScanner = new Scanner(System.in); //gets user input
  int choice; //creates int choice to determine if the user wants another story or not 
  String subject = subject(randomGen); //used in one of the cases in place of "it"
  do{ //do while loop
    System.out.println("The " + adjective(randomGen) + " " +  adjective(randomGen) + " " + subject(randomGen) + " " + 
                     verb(randomGen) + " " + "the " + adjective(randomGen) + " " + object(randomGen)); //phase 1 print statement
    System.out.println("This " + subject(randomGen) + " was " + adjective(randomGen) + " " + adjective(randomGen) + " " + " to " 
                       + adjective(randomGen) + " " + object(randomGen)); //print statememt 
    if(randomGen.nextInt(2) == 0){ //50% chance of "it" being used randomly
      subject = "It"; //assigns "it" to subject if the randomly produced number is 0
      System.out.println("It" + " used " + object(randomGen) + " to " + verb(randomGen) + " " + object(randomGen) + " at the " + adjective
      (randomGen) + " " + object(randomGen)); //print statement where the sentence uses "it" as the subject 
    
    }
    else{ //otherwise, it will just use a subject assigned to the string subject 
      System.out.println((subject) + " used " + object(randomGen) + " to " + verb(randomGen) + " " + object(randomGen) + "at the " + adjective
      (randomGen) + " " + object(randomGen)); //print statement 
     
    }
    System.out.println("That " + subject(randomGen) + " " + verb(randomGen) + " her " + object(randomGen)); //conclusion
    System.out.println("Enter 1 if you would like another sentence or 0 to exit the loop ");
    choice = myScanner.nextInt();//assigns the users choice to the variable choice                    
  
  } while(choice != 0); //if the user enters 0 then the loop is over 
  
  }
  
  
  

  public static String adjective(Random randomGen){
    int randomInt = randomGen.nextInt(10); //generates a random int between 0-10
    switch(randomInt){ //switch statements for different adjectives 
      case 0:
          return "cool";
      case 1:
        return "smart";
      case 2: 
        return "funny";
      case 3:
        return "happy";
      case 4:
        return "sassy";
      case 5:
        return "angry";
      case 6:
        return "sad";
      case 7:
        return "fat";
      case 8:
        return "gentle";
      default: //case 9
        return "hydrated";
        
    }
  
  }
  
  public static String subject (Random randomGen){
    int randomInt = randomGen.nextInt(10); //generates a random int between 0-10
        switch(randomInt){ //switch statements for different subjects 
      case 0:
          return "dog";
      case 1:
        return "bunny";
      case 2: 
        return "cat";
      case 3:
        return "fox";
      case 4:
        return "bear";
      case 5:
        return "lion";
      case 6:
        return "tiger";
      case 7:
        return "elephant";
      case 8:
        return "kangaroo";
      default: //case 9
        return "hippo";
        
    }
  
  }
  

  
  public static String verb (Random randomGen){
    int randomInt = randomGen.nextInt(10); //generates a random int between 0-10
        switch(randomInt){ //switch statements for different verbs 
      case 0:
          return "ran";
      case 1:
        return "thought";
      case 2: 
        return "walked";
      case 3:
        return "talked";
      case 4:
        return "ate";
      case 5:
        return "cried";
      case 6:
        return "laughed";
      case 7:
        return "drank";
      case 8:
        return "danced";
      default: //case 9
        return "kissed";
        
    }
  
  }
  public static String object(Random randomGen){
    int randomInt = randomGen.nextInt(10); //generates a random int between 0-10
    switch(randomInt){ //switch statements for different objects 
      case 0:
          return "chair";
      case 1:
        return "water bottle";
      case 2: 
        return "pencil";
      case 3:
        return "phone";
      case 4:
        return "table";
      case 5:
        return "credit card";
      case 6:
        return "ring";
      case 7:
        return "basement";
      case 8:
        return "cup";
      default://case 9
        return "forest";
        
    }
  
  }
  
  }