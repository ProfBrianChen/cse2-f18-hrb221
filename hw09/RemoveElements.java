//cse02 section 210 Helen Borchart. This class uses 3 different methods (randomInput, Delete, Remove) to fill an array
//with the length of 9 with random integers between 0-9 and then remove a specified location of the array and then a 
//target value from the array. 

import java.util.Scanner;
public class RemoveElements{
public static void main(String [] arg){
Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
 String answer="";
 do{
   System.out.print("Random input 10 ints [0-9]");
   num = randomInput();
   String out = "The original array is:";
   out += listArray(num);
   System.out.println(out);
 
   System.out.print("Enter the index ");
   index = scan.nextInt();
   newArray1 = delete(num,index);
   String out1="The output array is ";
   out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
   System.out.println(out1);
 
      System.out.print("Enter the target value ");
   target = scan.nextInt();
   newArray2 = remove(num,target);
   String out2="The output array is ";
   out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
   System.out.println(out2);
    
   System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
   answer=scan.next();
 }while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
 String out="{";
 for(int j=0;j<num.length;j++){
   if(j>0){
     out+=", ";
   }
   out+=num[j];
 }
 out+="} ";
 return out;
  }
  
public static int [] randomInput(){ 
  int [] array = new int [10]; //creates new array, it will always be 10 in length
  for(int i = 0; i < array.length; i++){ //loop to iterate through the array
    int random = (int) (Math.random() * 10); //generates random number between 0-9
    array[i] = random; //fills the array with the randomly generated numbers 
  }
  return array; //returns the array 
  }

public static int [] delete (int [] list, int pos){
  int [] newArray = new int[list.length-1]; //creates a new array that is one less in length then the original array
    if(pos < 0 || pos >= list.length) { //return the original list if the pos is invalid 
        return list;
    }
    int j = 0; 
    for(int i = 0; i < list.length; i++) { //iterates through original list
        if(i == pos) { //if the position in the loop is = to the target value i increments   
        } //fix*********
        else{
        newArray[j++] = list[i]; //the new val in the array is equal to the val at the position found at i
        }
    }
    return newArray;
}
 

public static int [] remove (int [] list, int target){ 
 int counter = 0; 
for (int i = 0; i < list.length; i++ ) { //iterates through the array
  if(list[i] == target) { //if the val at i does not equal the target you add to the counter to determine the length of the new array
      counter += 1;
    }
  }
  int[] newArray = new int[list.length-counter]; //length of new array determined from counter 
  for ( int i = 0, j = 0; i < list.length; i++ ) { //iterates through list 
    if(list[i] != target) { //checks to see if it is not equal to target 
      newArray[j] = list[i]; //value in new array is set 
      j++; 
    }
  }
  return newArray;
}


}