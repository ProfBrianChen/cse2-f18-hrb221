//cse02 section 210 Helen Borchart. Uses a scanner to prompt a user to enter 15 integers 0-100 and then uses a binary 
//and linear search to find if the target value is in the array. 
import java.util.Scanner;
import java.util.Random;
public class CSE2Linear{ 
public static void main(String[] args) {
Scanner myScanner = new Scanner(System.in); //creates scanner
int userInput = 0; //creates variable to hold user input
String junk = ""; //creates a string to hold junk if the user does not enter valid input
System.out.println("Enter 15 ascending ints for final grades in CSE 2 ");
int [] array = new int [15];
boolean sort = true;
for(int i = 0; i < array.length; i++){ //iterates through array 
  System.out.println("Enter a grade");
  while(!myScanner.hasNextInt()){ //checks to see if it is a string 
    System.out.println("Error, enter an int"); //error 
    System.out.println("Enter a grade");
    myScanner.next(); //clears buffer 
  }
  userInput = myScanner.nextInt(); //gets input 
  
  while(userInput < 0 || userInput > 100){ //checks to see that the int is in range 
    System.out.println("Error, enter an int in range"); //error 
    System.out.println("Enter a grade");
    userInput = myScanner.nextInt(); 
  }
  
  if(i > 0){
    while(array[i-1] > userInput){ //checks to see if the int is greater than or equal to the last int entered 
      System.out.println("Enter an integer greater than or equal to the last int entered");
      System.out.println("Enter a grade");
      userInput = myScanner.nextInt(); //clears buffer 
    } 
  }
  array[i] = userInput; //correct user input is assigned to userInput 
}

for(int j = 0; j < array.length; j++) //loop that goes through the array
  System.out.print(array[j] + " ");  //prints out the array 

System.out.println("Enter a grade to search for "); //prompts user to enter key
int key = myScanner.nextInt(); //sets key to the user input
System.out.println(binarySearch(key, array)); //calls the binary search method

randArray(array); //calls the randArray method 
System.out.print("Scrambled "); //print statement before the scrambled array
for (int j = 0; j < array.length; j++){ //loop that goes through array
System.out.print(array[j] + " "); //prints array
}
System.out.println(); //new line


System.out.println("Enter a grade to search for "); //prompts user to enter key
int linearKey = myScanner.nextInt(); //sets user input to the variable
System.out.println(linearSearch(array, linearKey)); //calls the linear search method
  }

public static boolean binarySearch(int key, int [] binaryArray) {
int numItems = 15; //num items in the array will always be 15
int iterations = 0; //initializes num iterations to 0
int low = 0; //low val is set to 0
int high = numItems - 1; //high val is initialized 
while(high >= low) { //while loop that goes through as long as high is greater than low 
int middle = (low + high) / 2; //finds the middle val
if(binaryArray[middle] == key){ //if the middle val is = to key then the key is found 
  return true; //returns true since key is found 
}
if(binaryArray[middle] < key) //checks other cases
  low = middle  +1;
if(binaryArray[middle] > key) //checks other cases
  high = middle -1;
iterations++; //adds to iterations  
}
System.out.println(key + " was not found in the list with " + iterations + " iterations"); //print statement
return false; //returns false since key was not found 
}





public static void randArray(int[] array){ //method that shuffles the array
Random random = new Random();//random method
for (int i = 0; i < array.length; i++){ //loop that goes through the length of the array
int index = random.nextInt(i + 1); //sets the index to a random num at the spot next to i
int a = array[index]; //sets a to the random number set at index
array[index] = array[i]; //swaps
array[i] = a; //swaps
    }
}   
public static boolean linearSearch(int[] array, int key){ //boolean method that uses a linear search to determine if the key is in the array
int iterations = 0; //variable that keeps track of the num of iterations
for(int i = 0;i < array.length; i++){    //loop that goes through the array
  if(array[i] == key){  //if the array at i is = to key then it returns true
  return true;
  }
iterations++; //adds to the num iterations 
}
System.out.println(key + " was not found in the list with " + iterations + " iterations"); //print statement 
return false; //returns false if it is not found
}        
}   

