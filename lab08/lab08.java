//cse02 section 210 Helen Borchart November 7, 2018, Lab08. This uses two arrays and one randomly is assigned
//100 random integers and the other array holds the number of occurences of each number in the first array.
import java.util.Random;
public class lab08{
  
  public static void main(String []args){
    final int ARRAY = 100;
    int[] randomInt = new int [ARRAY]; //array that holds the integers randomly assigned 
    int [] counter = new int[ARRAY]; //array that counts how many times the number came up
    Random rand = new Random(); //creates random object 
    
    for(int i = 0; i < ARRAY; i++){ //loop that goes through and assigns a random number 0-100 to the array 
      randomInt[i] = rand.nextInt(100);  //assigns the int
    }
 
    for(int i = 0; i < ARRAY; i++){ //counter array 
      counter[randomInt[i]]++;   //adds to the counter 
    }
    
    for(int i =0; i < ARRAY; i++) //loop for print statement 
      System.out.println(i + " occurs " +  counter[i] + " times"); //print statement 
   

  }

}