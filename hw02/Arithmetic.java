//cse 02. September 11, 2018. Helen Borchart. section 210. This class manipulates variables and works to calculate and print the cost of different items with and without the sales tax of 6% in Pennsylvania.

public class Arithmetic{
   public static void main(String[]args){
     int numPants = 3; //this variable is an int value representing the number of pants
     double pantsPrice = 34.98; //this variable represents the cost of a pair of pants
     int numShirts = 2; //this variable represents the number of shirts 
     double shirtPrice = 24.99; //this represents the cost of a shirt 
     int numBelts = 1; //this variable represents the number of belts
     double beltPrice = 33.99; //this variable represents the cost of belts 
     double paSalesTax = 0.06; //this variable represents the sales tax in Pennsylvania
     double totalCostOfPants; //total cost of the pants
     double totalCostOfShirts; //total cost of shirts
     double totalCostOfBelts; //total cost of the belts
     double salesTaxPants; //the sales tax on buying pants 
     double salesTaxShirt; //the sales tax on buying shirts 
     double salesTaxBelt; //the sales tax on buying belts 
     double totalCostBeforeTax; //the cost of the items before tax 
     double totalSalesTax; //the total amount of the sales tax
     double finalTotal; //the total of all the items including sales tax 
     
     totalCostOfPants = (numPants * pantsPrice); //computes the total cost of pants before sales tax 
     System.out.println("the total cost of pants before sales tax is $" + totalCostOfPants); //prints the pants total before tax
     totalCostOfShirts = (numShirts * shirtPrice); //computes the total cost of shirts before sales tax 
     System.out.println("the total cost of shirts before sales tax is $" + totalCostOfShirts); //prints the shirt total before tax 
     totalCostOfBelts = (numBelts * beltPrice); //computes the total cost of belts before tax 
     System.out.println("the total cost of belts before sales tax is $" + totalCostOfBelts); //prints the belt total before tax 
     salesTaxPants = (int)(((totalCostOfPants * paSalesTax)) * 100)/(100.0); //computes the sales tax on pants 
     System.out.println("the sales tax on pants is $" + salesTaxPants); //prints the sales tax on pants 
     salesTaxShirt = (int)(((totalCostOfShirts * paSalesTax)) * 100)/(100.0); //computes the sales tax on a shirt
     System.out.println("the sales tax on shirts is $" + salesTaxShirt); //prints out the sales tax on shirts 
     salesTaxBelt = (int)(((totalCostOfBelts * paSalesTax)) * 100)/(100.0); //computes the sales tax on belts 
     System.out.println("the sales tax on belts is $" + salesTaxBelt); //prints out the sales tax on belts 
     totalCostBeforeTax = (totalCostOfPants + totalCostOfBelts + totalCostOfShirts); //computes the total cost of the items before sales tax
     System.out.println("the total cost of the items before tax is $" + totalCostBeforeTax); //prints out the total cost of items before sales tax
     totalSalesTax = (int)(((salesTaxShirt + salesTaxPants + salesTaxBelt)) * 100) / (100.0); //computes the total of all the sales tax
     System.out.println("the total cost of all the sales tax is $" + totalSalesTax); //prints out the total cost of all the sales tax
     finalTotal = (int)((totalSalesTax + totalCostBeforeTax) * 100)/(100.0); //computes the total of the clothes including sales tax
     System.out.println("the total of all the items including sales tax is $" + finalTotal); //prints out the total of all the items including sales tax 
     
     
   }
  
}
    
  
    
  