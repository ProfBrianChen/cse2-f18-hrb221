//cse02. section 210. Helen Borchart. September 18, 2018. This Convert class uses the scanner class to take user input to convert
//the number of acres of land and the amount of rain that fell into cubic miles. 

import java.util.Scanner; //imports the scanner class 

public class Convert{
  public static void main(String args[]){
    Scanner myScanner = new Scanner(System.in); //creates a scanner called myScanner 
    System.out.println("Enter a double that represents the numbers of acres of land that was affected by hurricane precipitation"); 
    double numAcres = myScanner.nextDouble(); //assigns the user input to a double value called numAcres 
    System.out.println("Enter how many inches of rain were dropped on average");
    double numInches = myScanner.nextDouble(); //assigns the user input to a double value called numInches 
    double gallonsPer1AcreandIn = 27154.3; //the number of gallons of water in 1 acre of land with 1 inch of rain fall.  
    double cubicMilesPerGallon = 9.0817E-13; //the conversion between gallons and cubic miles 
    double finalRain = (numInches * gallonsPer1AcreandIn * cubicMilesPerGallon); //multiplying the users input for the inches of rain by the conversion factors 
    double finalAnswer = finalRain * numAcres; //multiplying the amount of rain y the number of acres to get the final amount of rainfall 
    System.out.println(finalAnswer + " cubic miles"); //a print system stating the number of cubic miles of rain based on the conversions 
      
    
    
  }
}