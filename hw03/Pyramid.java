//cse 02 section 210. Helen Borchart. This class prompts the user to enter dimensions of a pyramid through the scanner class then
//computes the volume inside the pyramid

import java.util.Scanner; //imports the scanner class 

public class Pyramid{
  public static void main(String args[]){
    Scanner myScanner = new Scanner(System.in); //creates a scanner named myScanner 
    System.out.println("The square side of the pyramid is "); 
    double squareSide = myScanner.nextInt(); //assigns user input to the variable squareSide 
    System.out.println("The height of the pyramid is ");
    double height = myScanner.nextInt(); //assigns the users height input to the variable height 
    double squareArea = (squareSide * squareSide); //calculates the area of the square base in the pyramid 
    double squareAreaHeight = squareArea * height; //multiplies the squareArea * the height, which is the next step in calculating the volume of a pyramid 
    double volume = squareAreaHeight/3; //divides the squareAreaHeight by 3, which is part of the formula for volume of a pyramid 
    System.out.println("The volume inside the pyramid is: " + volume); //prints out the volume of a pyramid
  }
  
}