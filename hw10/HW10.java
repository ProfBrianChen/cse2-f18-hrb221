//cse02 section 210 Helen Borchart. This program uses a 2d array of characters to simulate a game of tic tac toe.

import java.util.Scanner;

public class HW10 {
  public static void main(String [] args) {
    Scanner myScanner = new Scanner(System.in);
    int pos = -1;
    int row = 0;
    int col = 0;
    char player = 'O';
    char[][] array = new char[3][3]; //creates an array of char to hold the tic tac toe game 
    char num = '1'; //number that is the character in the array 
    for (int i = 0; i < 3; i++){ //nested for loops to fill the 2d array 
      for (int j = 0; j < 3; j++) {
        array[i][j] = num++; //fills array with numbers 1-9
      }
    }
    printArray(array);
    while(!winner(array) && hasEmptySpot(array)){ //while there is an empty spot or there is not a winner 
      System.out.println("Enter the number position you want to play for player " + player + ":"); //sorry this is
      //confusing wording I'm not sure how to say this but I hope it makes sense
      pos  = myScanner.nextInt(); 
      while(pos < 1 || pos > 9){ //checks to see that the int is in range 
        System.out.println("Error, enter an int in range"); //error 
        pos  = myScanner.nextInt(); 
      }     
      if(pos > 0 && pos < 4){ //determines the row depending on the user input 
       row = 0; 
      }
      else if(pos > 3 && pos < 7) { //determines the row depending on the user input 
        row = 1;   
      }
      else{
        row = 2;  //default 
      }
      
      if(pos%3 == 1){ //determines the column 
       col = 0; 
      }
      else if(pos%3 == 2) { //determines the column 
        col = 1;   
      }
      else{
        col = 2;
      }    
      while (array[row][col] == 'X' || array[row][col] == 'O') { //while loop that checks if the spot is taken 
        System.out.println("This spot is taken. Try another spot");
        row = myScanner.nextInt(); //gets new input 
        col = myScanner.nextInt();
            }
      array[row][col] = player; //switches the value in the specified location to the player who just played 
      printArray(array); //returns the array after a player has taken their turn 
      if (winner(array)){ //calls method that checks if there is a winner 
        System.out.println("Player " + player + " is the winner!");
      }
      else if (player == 'O') { //switches players 
        player = 'X';
      }
      else {
        player = 'O'; 
           }
    }
    if (winner(array) == false) { //since there are no empty spots and there is no winner (determines w the while loop 
      //in line 20, the only option is that there is a draw 
        System.out.println("The game is a draw. Please try again.");
      }
  }
    public static void printArray (char [] [] array){ //display array method 
        for (int i = 0; i < array.length; i++) { //nested for loops that goes through the array 
            for (int j = 0; j < array[i].length; j++) {
                if (j == array[i].length - 1) System.out.print(array[i][j]); //prints array  
                else System.out.print( array[i][j] + "  "); //prints array 
            }
            System.out.println();
        }
    }    
    
public static boolean hasEmptySpot(char[][] array){ //method that checks if the array has any spots (in the event of a draw) 
    for (int i = 0; i< array.length; i++) { //nested for loops go through the array 
        for (int j = 0; j < array[0].length; j++) {
          if (array[i][j] != 'O' && array[i][j] != 'X') { //check
                return true; //boolean return true 
            }
        }
    }
    return false; //the array does not have empty spots 
}
    
   
public static boolean winner(char[][] array){ //method that determines if there is a winner i.e. 3 x's or o's in a row/column/diagnol
   boolean occupied = true; 
    for (int i = 0; i< array.length; i++) { //nested loops to go through 
        for (int j = 0; j < array[0].length; j++) {
            if (array[i][j] != 'O' || array[i][j] != 'X') { //checks if there is an empty spot
                occupied = false; //sets occupied to false 
            }
        }
    }
    if(occupied) //there is not a winner in this case
      return false;
    else //checks the 8 possible cases of winning tic tac toe
      return (array[0][0] == array [0][1] && array[0][0] == array [0][2]) ||
      (array[0][0] == array [1][1] && array[0][0] == array [2][2]) ||
      (array[0][0] == array [1][0] && array[0][0] == array [2][0]) ||
      (array[2][0] == array [2][1] && array[2][0] == array [2][2]) ||
      (array[2][0] == array [1][1] && array[0][0] == array [0][2]) ||
      (array[0][2] == array [1][2] && array[0][2] == array [2][2]) ||
      (array[0][1] == array [1][1] && array[0][1] == array [2][1]) ||
      (array[1][0] == array [1][1] && array[1][0] == array [1][2]);
}
}