//cse 02 welcome class. Helen Borchart. Sept 2, 2018. 

public class WelcomeClass{
  public static void main(String args[]){
    //system prints welcome with a lehgih id to terminal 
    System.out.println("-----------");
      System.out.println("| WELCOME |");
        System.out.println("-----------");
          System.out.println("^  ^  ^  ^  ^  ^");
            System.out.println("/ \\/ \\/ \\/ \\/ \\/ \\");
              System.out.println("<-H--R--B--2--2--1->");
                System.out.println("\\ /\\ /\\ /\\ /\\ /\\ /");
                  System.out.println("v  v  v  v  v  v ");
                    System.out.println("My name is Helen Borchart and I am a sophomore at Lehigh University majoring in Cognitive Science with a minor in CS. Fun fact is I have a twin sister who is a CS major who goes to the University of Richmond");
  }
}
