//cse02. Section 210. Helen Borchart. September 19, 2018. This class uses the Math.random to pick a random card from a deck of cards.

import java.util.Random;

public class CardGenerator {
  public static void main (String args[])
  {
  String suitName = " ";
  String cardID = " ";
   
  int card =(int) (Math.random() * (52 + 1) + 1); //generates random number between 1 and 52
  if(card  >= 1 && card <= 13) //checks to see if the int value is between 1 and 13
     suitName = "Diamonds"; //assigns suit to diamonds
  else if (card >= 14 && card <= 26) //checks to see if the int value is between 14 and 26
    suitName = "Clubs"; //assigns suit to clubs 
   else if (card >= 27 && card <= 39) //checks to see if the int value is between 27 and 39
     suitName = "Hearts"; //assigns the suit name to hearts 
    else
      suitName = "Spades"; //assigns suit name to spades 
    
    int val = card % 13; //uses modules to determine what type of card is drawn 
    switch(val) //switch statement 
    {
      case 1:  //checks to see if the modulus is 1
        cardID = "ace"; //assigns cardID to ace 
        break;
      case 0: //checks to see if the modulus is 0
        cardID = "king"; //assigns cardID to king 
        break;
      case 12: //checks to see if the modulus is 12
        cardID = "queen"; //assigns cardID to queen 
        break;
      case 11: //checks to see if the modulus is 11
        cardID = "jack"; //assigns cardID to jack
        break;
      default:
        cardID = "" + val; //assigns card ID to the value of the card drawn 
        
    }
   
    System.out.println("you drew a " + cardID + " of " + suitName); //prints out the cardID and the suit type
  }
}
  