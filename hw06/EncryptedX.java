//cse02 Helen Borchart. This program uses loops and if statements to print a hidden x in a star pattern. 
import java.util.Scanner;
public class EncryptedX{
  public static void main(String []args){
    Scanner myScanner = new Scanner(System.in);
    int userInput = 0;
    String junk = "";
    System.out.println("Enter a number between 0 - 100");
if(myScanner.hasNextInt()){ //loop that executes if the scanner receives bad input
  if(userInput < 0 || userInput > 100){
    junk = myScanner.nextLine(); //clears buffer
    System.out.println("Error, enter an integer within range");
  
  }
  else if(!myScanner.hasNextInt())
  System.out.println("Error, enter an int");
  }
while(myScanner.hasNextInt()){//ensures that the proper user input is entered
  userInput = myScanner.nextInt(); //assigns the users input to the variable user input 
  for (int i = 0; i <= userInput; i++){ //loop that makes sure that the pattern prints out the amount of rows that the 
    //user enters
    for(int j = 0 ; j <= userInput; j++){ //loop that controls what is printed out inside each row
        if(j == i || j == userInput - i) //if the position is equal to the user input or the position subtracted from 
          //the input, a space is printed. This is to create the x pattern.
          System.out.print(" "); //print statement for a space
        else
          System.out.print("*"); //otherwise, a star is printed.
        
  }
    System.out.println(); //creates a new line
}
  break; //breaks out of the loop
  
}

  }
  
}