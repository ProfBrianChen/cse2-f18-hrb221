//cse02 section 210 helen borchart. this uses nested for loops to print out a variation of different pattern pyramids.
//this pyramid is meant to print in descending order i.e. 6 5 4 3 2 1, 5 4 3 2 1, ending with only 1. 

import java.util.Scanner;
public class PatternD{
  public static void main (String[]args){
    Scanner myScanner = new Scanner(System.in); //creates scanner
    int userInput = 0; //creates variable to hold user input
    String junk = ""; //creates a string to hold junk if the user does not enter valid input
    System.out.println("Enter an int 1-10");
    while(!myScanner.hasNextInt()){ //loop that executes if the scanner receives bad input
      junk = myScanner.nextLine(); //clears buffer
      System.out.println("Error, enter an integer"); 
    }
    while(myScanner.hasNextInt()){ //loop that executes if there is valid input
      userInput = myScanner.nextInt(); //assigns the user input to the variable
      if(userInput< 1 || userInput > 10) //checks to see that the user input is between 1 and 10
      System.out.println("Error, enter an int between 1-10"); //error message if the input is not between 1- 10
      if(userInput >= 1  && userInput <= 10){ //if statement that executes if the input is valid 
        break;  //breaks out of the loop
      }
        
    }
for(int i = userInput; i >= 1; i--) //for loop that controls the number of rows there will be 
{
  for(int j = i; j >= 1; j--) //loop that controls the output
    System.out.print(j + " "); //print statement 
    System.out.println(); //new line 
  }
}
  
  
  }
 