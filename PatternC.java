//cse2 section 210. Helen Borchart. This uses nested loops to print out variations of different patterns. This is pattern
//c and uses another loop to ensure that the pattern prints out with the correct spacing. tje pattern starts to the left
//and moves to the right 

import java.util.Scanner; //imports the scanner 
public class PatternC{
  public static void main (String[]args){
    Scanner myScanner = new Scanner(System.in); //creates scanner
    int userInput = 0; //creates variable to hold user input
    String junk = ""; //creates a string to hold junk if the user does not enter valid input
    System.out.println("Enter an int 1-10");
    while(!myScanner.hasNextInt()){ //loop that executes if the scanner receives bad input
      junk = myScanner.nextLine(); //clears buffer
      System.out.println("Error, enter an integer"); 
    }
    while(myScanner.hasNextInt()){ //loop that executes if there is valid input
      userInput = myScanner.nextInt(); //assigns the user input to the variable
      if(userInput< 1 || userInput > 10) //checks to see that the user input is between 1 and 10
      System.out.println("Error, enter an int between 1-10"); //error message if the input is not between 1- 10
      if(userInput >= 1  && userInput <= 10){ //if statement that executes if the input is valid 
        break;  //breaks out of the loop
      }
        
    }
for (int i = 1; i <= userInput; i++) { //the outer loop that controls how many rows there will be 
  for (int j = userInput; j > i; j--) { //controls the spacing 
    System.out.print(" ");
        }
  for (int k = i; k >= 1; k--){ //loop that controls the output that is printed 
    System.out.print(k + "");
        }
  System.out.println();//prints a new line 
    }
}
}