//cse02 section 210 Helen Borchart. This class takes in various inputs of strings and ints and discards the information if it is not of the correct type.

import java.util.Scanner; //imports scanner class 

public class Lab05{
  public static void main(String []args){
    Scanner myScanner = new Scanner(System.in);
    int courseNumber; //linea 8 - 13 declare different variables
    String departmentName = "";
    int numMeetingTimes;
    int courseTime;
    String instructorName = "";
    int numStudents;
    
    System.out.println("Enter the course number"); //prompts user to enter input
    while(!myScanner.hasNextInt()) //loop that checks to see if the input is NOT an int
    {
   System.out.println("Error, enter an integer"); //since the user input is not an int an error is printed 
   myScanner.nextLine(); //clears the buffer 
    }
  courseNumber = myScanner.nextInt(); //if it is an int, course number is assigned to the user input
    System.out.println("Enter the department name"); //prompts user to enter a string for the department name 
    myScanner.nextLine(); //clears buffer
    departmentName = myScanner.nextLine(); //assigns department name to the input
    System.out.println("Enter how many times you meet"); //promps user to enter int input
    while(!myScanner.hasNextInt()) //loop that checks to see if the input is NOT an int
    {
      System.out.println("Error, enter an integer"); //error message if the input is not an int and the loop executes
      myScanner.nextLine(); //clears buffer 
      
     }
    numMeetingTimes = myScanner.nextInt(); //assigns input to numMeetingTimes if it is an int 
    System.out.println("Enter what time you meet"); //prompts user to enter input
    while(!myScanner.hasNextInt()) //loop that checks to see if input is not an int
    {
    System.out.println("Error, enter an int"); //error message if loop executes 
    myScanner.nextLine(); //clears buffer
      
    }
    courseTime = myScanner.nextInt(); //assigns courseTime to user input since it is the correct type of input
    System.out.println("Enter the instructor name"); //prompts user to enter input
    myScanner.nextLine(); //gets input
    instructorName = myScanner.nextLine(); //assigns instructorName to user input
  
    System.out.println("Enter the number of students"); //prompts user to enter input
    while(!myScanner.hasNextInt()) //loop that executes if input is not the correct type 
    {
   System.out.println("Error, enter an integer"); //error message
   myScanner.nextLine(); //clears buffer
    }
  numStudents = myScanner.nextInt(); //assigns numStudents to the correct input from user 
    
    System.out.println("Your course number is " + courseNumber + " Your department name is " + departmentName 
                      + " Your number of meeting times is " + numMeetingTimes + " Your course time is " + courseTime
                      + " Your instructor name is " + instructorName + " The number of students in your class " + numStudents);
   
   
  }
}