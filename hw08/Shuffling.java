//cse02 Helen Borchart section 210. This program uses different methods and arrays to print out a deck of cards, shuffles the cards, 
//prints out the shuffled deck, then printing out a hand of cards
import java.util.Scanner;
public class Shuffling{ 
public static void main(String[] args) { 
Scanner scan = new Scanner(System.in); 
String junk = ""; //holds bad userInput
int userInput = 0;
do{
System.out.println("Enter an int 1-52");
while(!scan.hasNextInt()){ //loop that executes if the scanner receives bad input
  junk = scan.nextLine(); //clears buffer
  System.out.println("Error, enter an integer"); //print statement 
  scan.next();
   }
userInput = scan.nextInt();
}while(userInput < 1  || userInput > 52); 
int numCards = userInput;
 //suits club, heart, spade or diamond 
String[] suitNames={"C","H","S","D"};    
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
String[] cards = new String[52]; 
String[] hand = new String[5];  
int again = 1; 
int index = 51;
for (int i=0; i<52; i++){ 
  cards[i]=rankNames[i%13]+suitNames[i/13]; 
} 
System.out.println();
printArray(cards); 
shuffle(cards); 
printArray(cards); 
while(again == 1){ 
   hand = getHand(cards,index,numCards); 
   printArray(hand);
   index = index - numCards;
   if(index - numCards <= 0){ //this checks to see if numCards is greater than the number of cards in the deck then creates
     //a new deck
     shuffle(cards);
   index = 51; //sets index to 51
   }
   System.out.println("Enter a 1 if you want another hand drawn"); //print statement 
   again = scan.nextInt(); 
}
}


public static void printArray(String [] list){ //prints array
  for(int i = 0; i < list.length; i++) //loop to go through print array
    System.out.print(list[i] + " "); //print statement
  System.out.println();
}

public static String[] getHand(String[]list, int index, int numCards){ //method to get a hand of cards 
  System.out.println("Hand");
  String [] numHandsDeck = new String [numCards]; //creates an array the length of numCards
    for(int i = 0; i < numHandsDeck.length; i++, index--){ //for loop to go through numHandsDeck
    numHandsDeck[i] = list[index]; //sets numHandsDeck at the position in the loop to the list at the index position 
  }
  return numHandsDeck; //returns the array of the numHandsDeck
}
  
public static String[] shuffle(String [] list){ //method that shuffles the cards 
  System.out.println("Shuffled"); //print statement before the shuffled deck is printed 
    for(int i = 0; i < 100; i++){ //we had to pick a number over 50 so I just picked 100 times for it to be shuffled
    int random = (int) (Math.random() * 51) + 1; //generats a random number between 1-52
    String temp = list[0]; //swap
    list[0] = list[random]; //swap
    list[random] = temp; //swap
  }
  return list; //returns the shuffled deck array

}
}