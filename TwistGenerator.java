//cse02 section 210. Helen Borchart. Due Oct 9th. This program uses if and loops to generate twists 
//depending on how many spaces are entered).

import java.util.Scanner; //imports scanner
public class TwistGenerator{
public static void main(String [] args ) {
Scanner myScanner = new Scanner(System.in);

int pos = 1; //creates a variable that keeps track of the position of the pattern that the loop is at 
int length; //variable to hold the user input of how long the twist is 
String junk = ""; //variable to hold incorrect input
int line = 1;

System.out.println("Enter an integer greater than 0"); //prompts user for input
while(!myScanner.hasNextInt()){ //checks to see if the user did NOT enter a valid input (int)
  System.out.println("Error, please enter an integer greater than 0"); //print statement asking user to enter correct input
  junk = myScanner.next(); //clears buffer 
}
length = myScanner.nextInt(); //assigns user input to variable length

for (line = 1; line <= length; line++){ //for loop that goes through the first line of code 
  if(pos == 1){ //if the position of the pattern is at 1, then the loop will execute the first pattern
    System.out.print("\\"); //print statememt 
    pos = 2; //moves the position to the next pattern
  }
  else if(pos == 2) //executes the next statement 
  {
    System.out.print(" "); //print statement 
    pos = 3; //moves the position to the next pattern by changing position to 3
  }
  else if(pos == 3) //if statement that checks if position = 3
  {
    System.out.print("/"); //print statement 
    pos = 1; //resets pos to 1 
    
  }
    
}
System.out.println(""); //prints new line
pos = 1;//resets pos to 1
for (line = 1; line <= length; line++){ //for loop that executes the second pattern 
  if(pos ==1) //checks to see pos is at 1 
  {
    System.out.print(" "); //print statement for the first part of the pattern 
    pos = 2; //sets pos to 2
  }
  else if(pos == 2) //checks to see that pos is 2 through an if statement 
  {
  System.out.print("X"); //print statement for the second part of the pattern 
  pos = 3; //sets pos to 3
  }
  else if(pos == 3) //if statement to see if pos is 3
  {
  System.out.print(" "); //print statement for the third part of the pattern 
  pos = 1; //sets pos to 1
  }
}
System.out.println(""); //new line
pos = 1; //resets pos to 1
for (line = 1; line <= length; line++){ //for loop that executes the third line of the pattern twist 
  if(pos == 1){ //if statement to see if pos is at 1
    System.out.print("/"); //print statement for the first part of the pattern 
    pos = 2; //sets pos to 2
  }
  else if(pos == 2) //checks to see if pos is at 2 with an if statememt 
  {
    System.out.print(" "); //print statement for the second part of the pattern 
    pos = 3; //sets pos to 3
  }
  else if(pos == 3) //if statement that sees if pos is at 3 
  {
    System.out.print("\\"); //print statement for the third part of the pattern 
    pos = 1; //sets pos to 1
    
   }

  }
 }
}

 