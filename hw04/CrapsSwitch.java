//cse02 section 210. Helen Borchart. this program simulates a Casino game, Craps, involving 
//two six sided dice. depending on what the results of the dice are, there are slang names
//for the results. this code uses switch statements to determine and print the slang name.


import java.util.Scanner; //imports the scanner class 

public class CrapsSwitch{
  public static void main(String args[]){ 
    int statedNum1; //decalres variable 
    int statedNum2; //declares variable 
    int diceRoll =(int) (Math.random() * (6) + 1); //declares variable and assigns it to a random int between 1 and 6 with the math.random class 
    int diceRoll2 = (int)(Math.random() * (6) + 1); 
    int diceRollSum = diceRoll + diceRoll2;
    Scanner myScanner = new Scanner(System.in);
    System.out.println("Enter 1 if you want to randomly cast the dice or enter 2 if you would like to state the dice face");
    int userInput = myScanner.nextInt(); //uses scanner to determine whether or not the user wants to pick the dice face or have it be selected randomly 
    switch(userInput) //switch statement 
    {
      case 1: //if the user enters 1, then the dice is rolled randomly
        System.out.println("You rolled a " + diceRoll + " and " + diceRoll2);
        switch(diceRollSum) //switch statement checking to see what the dice roll sum is 
        {
          case 2: //checks to see if the sum is 2
            System.out.println("You rolled Snake Eyes"); //prints out the correct name if the sum is 2
            break;
          case 3: //checks the sum 
            System.out.println("You rolled Ace Deuce"); //print statement 
            break;
          case 4: //checks the sum of the dice 
            switch(diceRoll) //depending on how the 4 is rolled, the name differs so this switch statement determines the case 
            {
              case 2: //if the dice is made up of a 2 and 2 then it is considered a hard four
                System.out.println("You rolled Hard Four"); //print statement 
                break;
              case 1: case 3: //if the dice rolled is a 1 or a 3 it is considered an easy four
                System.out.println("You rolled Easy Four");   
                break;
            }
            break;
          case 5: //if the sum is 5, this case statement exectues 
            System.out.println("You rolled Fever Five"); //print statement 
            break;
          case 6:
            switch(diceRoll) //depending on how the 6 is rolled, the name will be different 
            {
              case 3: //if it is a 3 and a 3, this statement executes
                System.out.println("You rolled Hard Six"); 
                break;
              case 5: case 4: case 1: case 2: //if the roll is made up of a 5 or a 4 (or 1 and 2), this statement executes
                System.out.println("You rolled Easy Six");   //print statement 
                break;
            }
            break;
          case 7: //if the dice sum is 7, this switch statement is executed 
            System.out.println("You rolled Seven Out");
            break;
          case 8: //if the dice roll sum is 8, this switch statement is executed 
            switch(diceRoll)
            {
              case 4: //if one of the dice rolled is a 4, this statement is executed 
                System.out.println("You rolled Hard Eight");
                break;
              case 5: case 6: case 3: case 2: //if one of the dices rolled is a 5 or 6 (or 3 or 2), this statement is executed
                System.out.println("You rolled Easy Eight");   
                break;
            }
            break;
          case 9: //if the dice roll sum is 9, this statement is executed 
            System.out.println("You rolled Nine");
            break;
          case 10: //switch statement if the dice roll sum is 10
            switch(diceRoll) //switch statement checking to see how the 10 was rolled 
            {
              case 5: //if one of the dice rolled is a 5, this statement executes 
                System.out.println("You rolled Hard Ten"); //print statement 
                break;
              case 6: case 4: case 2: case 8: //checks to see how the 10 was rolled
                System.out.println("You rolled Easy Ten");
                break;
            }
            break;
          case 11: //if the sum is 11, this switch statement is applied 
            System.out.println("You rolled Yo-leven"); //print statement 
            break;
          case 12: //if the sum is 12, this switch statement is applied 
            System.out.println("You rolled Boxcars"); //print statement 
            break;
        }
        break;
      case 2: //if the user inputs 2, this applies 
        System.out.println("Enter the two dice values"); //asks users to enter dice value 
        statedNum1 = myScanner.nextInt(); //uses scanner to get their input and store the first number as statedNum1
        statedNum2 = myScanner.nextInt(); //uses scanner to get input and store the second num in this variable 
        int statedNumSum = statedNum1 + statedNum2; //creates a variable for the sum of the numbers 
        switch(statedNumSum) //see comments from the first section. this is the exact same code just altered names for the option of users entering input instead of having it randomly done 
        {
          case 2:
            System.out.println("You rolled Snake Eyes");
            break;
          case 3:
            System.out.println("You rolled Ace Deuce");
            break;
          case 4:
            switch(statedNum1)
            {
              case 2: 
                System.out.println("You rolled Hard Four");
                break;
              case 3: case 1:
                System.out.println("You rolled Easy Four");   
                break;
            }
            break;
          case 5: 
            System.out.println("You rolled Fever Five");
            break;
          case 6:
            switch(statedNum1)
            {
              case 3: 
                System.out.println("You rolled Hard Six");
                break;
              case 4: case 5: case 1: case 2: 
                System.out.println("You rolled Easy Six");   
                break;
            }
            break;
          case 7:
            System.out.println("You rolled Seven Out");
            break;
          case 8:
            switch(statedNum1)
            {
              case 4: 
                System.out.println("You rolled Hard Eight");
                break;
              case 5: case 6: case 3: case 2:
                System.out.println("You rolled Easy Eight");   
                break;
            }
            break;
          case 9:
            System.out.println("You rolled Nine");
            break;
          case 10:
            switch(statedNum1)
            {
              case 5:
                System.out.println("You rolled Hard Ten");
                break;
              case 6: case 4: case 8: case 2:
                System.out.println("You rolled Easy Ten");
                break;
            }
            break;
          case 11:
            System.out.println("You rolled Yo-leven");
            break; 
          case 12:
            System.out.println("You rolled Boxcars");
            break; 
    }
        break;
    
    }
     
  }
  }