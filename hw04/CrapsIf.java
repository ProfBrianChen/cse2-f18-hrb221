//cse02 section 210. Helen Borchart. this program simulates a Casino game, Craps, involving 
//two six sided dice. depending on what the results of the dice are, there are slang names
//for the results. this code uses if statements to determine and print the slang name.
//I know this was an ineffective way of doing it but by the time I realized there was a less time consuming way
//I felt like I was too far into my original plan for writing the code this way

import java.util.Scanner; //imports the scanner class 

public class CrapsIf{
  public static void main(String args[]){  
    int statedNum1; //decalres variable 
    int statedNum2; //declares variable 
    int diceRoll =(int) (Math.random() * (6) + 1); //declares variable and assigns it to a random int between 1 and 6 with the math.random class 
    int diceRoll2 = (int)(Math.random() * (6) + 1); 
    Scanner myScanner = new Scanner(System.in);
    System.out.println("Enter 1 if you want to randomly cast the dice or enter 2 if you would like to state the dice face");
    int userInput = myScanner.nextInt(); //uses scanner to determine whether or not the user wants to pick the dice face or have it be selected randomly 
    if(userInput == 1) //if the user wants to randomly cast the dice they were told to enter 1 so this checks for that case 
    {
    System.out.println("You rolled a " + diceRoll + " and a " + diceRoll2); //prints out the results of the random dice 
     if(diceRoll + diceRoll2 == 2) //checks to see if the sum of the random results is = to 2
    {
     System.out.println("You rolled Snake Eyes"); //prints out the slang name 
    }
    if(diceRoll + diceRoll2 == 3) //checks to see if the sum of the random results is = to 3
    {  
    System.out.println("You rolled Ace Deuce"); //prints slang name 
    }
    if(diceRoll + diceRoll2 == 4) //checks to see if the sum of the results is = to 4
    {
      if(diceRoll == 2 && diceRoll2 == 2) //checks to see both dice are 2
        System.out.println("You rolled Hard Four"); //prints slang name when both dice are 2
      else
        System.out.println("You rolled Easy Four"); //prints slang name for result when the result is equal to 4 but not both dice are 2
    }
    if(diceRoll + diceRoll2 == 5) //checks to see if the sum is = to 5
    {
    System.out.println("you rolled Fever Five"); //prints out slang name 
    }
    if(diceRoll + diceRoll2 == 6) //checks to see if the sum = 6
    {
    if(diceRoll != diceRoll2) //depending on whether or not the result is made up of the same dice face rolled on both die, the name is then printed accordingly 
      System.out.println("You rolled Easy Six"); 
    else
      System.out.println("you rolled Hard Six");   
    }
    if(diceRoll + diceRoll2 == 7) //checks to see if the sum = 7 
    {
    System.out.println("you rolled Seven Out"); //print statement 
    }
    if(diceRoll + diceRoll2 == 8) //depending on whether or not the result is made up of the same number, the slang name is printed accordingly 
    {
    if(diceRoll != diceRoll2) 
      System.out.println("you rolled Easy Eight");
    else
      System.out.println("you rolled Hard Eight");
    }
    if(diceRoll + diceRoll2 == 9) //checks to see if the sum = 9
      System.out.println("you rolled Nine"); //print statement 
    if(diceRoll + diceRoll2 == 10) //depending on what the dice face is, the name is then printed accordingly 
    {
    if(diceRoll != diceRoll2)
      System.out.println("you rolled Easy Ten");
    else
      System.out.println("you rolled Hard Ten");
    }
   if(diceRoll + diceRoll2 == 11) //checks sum 
     System.out.println("you rolled Yo-leven"); //print statement 
   if(diceRoll + diceRoll2 == 12) //checks sum
     System.out.println("you rolled Boxcars"); //print statement 
    }
    if(userInput == 2) //does the same thing in the comments stated before, just calculates them based on the users input instead of random dice values 
    {
    System.out.println("Enter the two face numbers");
    statedNum1 = myScanner.nextInt();
    statedNum2 = myScanner.nextInt();
     if(statedNum1 + statedNum2 == 2)
    {
     System.out.println("You rolled Snake Eyes");
    }
    if(statedNum1 + statedNum2 == 3)
    {  
    System.out.println("You rolled Ace Deuce");
    }
    if(statedNum1 + statedNum2 == 4)
    {
      if(statedNum1 == 2 && statedNum2 == 2)
        System.out.println("You rolled hard four");
      else
        System.out.println("You rolled easy four");
    }
    if(statedNum1 + statedNum2 == 5)
    {
    System.out.println("You rolled Fever Five");
    }
    if(statedNum1 + statedNum2 == 6)
    {
    if(statedNum1 != statedNum2)
      System.out.println("You rolled Easy Six");
    else
      System.out.println("You rolled Hard Six");   
    }
    if(statedNum1 + statedNum2 == 7)
    {
    System.out.println("You rolled Seven Out");
    }
    if(statedNum1 + statedNum2 == 8)
    {
    if(statedNum1 != statedNum2)
      System.out.println("Rou rolled Easy Eight");
    else
      System.out.println("You rolled Hard Eight");
    }
    if(statedNum1 + statedNum2 == 9)
      System.out.println("You rolled Nine");
    if(statedNum1 + statedNum2 == 10)
    {
    if(statedNum1 != statedNum2)
      System.out.println("you rolled easy ten");
    else
      System.out.println("you rolled hard ten");
    }
   if(statedNum1 + statedNum2 == 11)
     System.out.println("you rolled yo-leven");
   if(statedNum1 + statedNum2 == 12)
     System.out.println("you rolled boxcars");
    }
      

  }
  
  }
  

  